#41秒了解集结号充值上分微信号xgp
#### 介绍
集结号充值上分微信号【溦:2203922】，集结号信誉上分微信【溦:2203922】，集结号游戏充值上分下分【溦:2203922】，集结号充值上分商人微信【溦:2203922】，集结号上分银商微信【溦:2203922】，所有法则都是天生的，都是命运，偶然相遇，突然回头，注定了彼此的生命，只是在相遇的那一刻。
　　敬仰的，我要的快乐就这么大略：愿得一民心白，首不相离！我的终身所愿，然而是你的眼底惟有我。初见的害羞，相恋的痛快，酷热的冲动，甘甜的憧憬，平常的快乐，古稀的相扶。而咱们牵的手，从未划分。寰球再大，有你，足矣！
　　农历十月初三，父亲的62岁生日。我们三个儿子掀起了“父亲批判”的高潮。屋外太阳出奇的好，初冬的乡村一遍温暖祥和。母亲在无奈的折磨中机器般地劳作，默默地将心中的怨气和眼泪自个儿吞下。儿女有儿女的事，有儿女的道理，有儿女的立场，能给予父亲的临终关怀除了床前苍白的问候和无语的看望，便是背后集体的批判和声讨。父亲在临死之前成了儿子们的敌人。母亲是一个牺牲，是一个殉葬品，没有独立的人格，整个人就是父亲多年塑造的。在无法承受父亲的肉体和精神折磨之后，母亲开始了苍凉的醒悟，但这醒悟怎么也无法让她有反叛的举动。木楼下的狗莫名其妙地死了，我们到家时刚断气。一个胖胖的乖狗，死后却显得非常地瘦和脏。当母亲从鸡圈里拖出一条死老鼠，才知道狗是吃了死老鼠中毒身亡的。二哥把死狗扔进了门前的涪江，木楼下又有洁净祥和的气息。父亲在亲戚面前数落着我们的罪过：没帮母亲划柴，没帮母亲挑水，没帮母亲灌园子。父亲的眼泪无尽地流淌着，揩也揩不完。我不知道一个死到临头的人怎么还有那么多的眼泪。我不知道父亲的眼泪里有没有癌。在二哥看来，父亲若真还有泪可流，流的也应该是忏悔的泪。我同意二哥的观点，父亲应该忏悔。我知道我们的观点很危险，很可能为我们的道德我们的社会所不齿。也许在我们的道德看来，要自己奄奄一息的父亲忏悔不仅是忤孽不肖，而且是心怀歹毒。

#### 参与贡献
Fork 本仓库
新建 Feat_xxx 分支
提交代码
新建 Pull Request
#### 特技
使用 Readme_XXX.md 来支持不同的语言，例如 Readme_en.md, Readme_zh.md
Gitee 官方博客 blog.gitee.com
你可以 https://gitee.com/explore 这个地址来了解 Gitee 上的优秀开源项目
GVP 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
Gitee 官方提供的使用手册 https://gitee.com/help
Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 https://gitee.com/gitee-stars/